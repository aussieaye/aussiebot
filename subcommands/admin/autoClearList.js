require(global.baseDir + "/utils/Array.prototype.clean");

const constants = require(global.baseDir + "/config/constants.js");
const notify = require(global.baseDir + "/utils/notify.js");
const petinfo = require(global.baseDir + "/config/petinfo.js");
const TransHash = require(global.baseDir + "/utils/TransHash.js");

module.exports = {
    options: {
        parent: "autoclear",
        name: "list",
        usage: "list",
        description: "Lists all raid being cleared daily",
        fullDescription: "Lists all raid being cleared daily",
        deleteCommand: true,
        hidden: true,
        category: constants.ABC_ADMIN,
        requirements: {
            custom: constants.permissionsCheck.bind({
                permissions: {
                    "administrator": true
                }
            })
        },
        permissionMessage: constants.permissionsMessage
    },

    exec: async function (bot, msg, args) {
        // Adding the raid to the list
        let raidList = global.mconf.get("raids:list");
        let display = "The following raids are being cleared automaticlly:\n\`\`\`\n";

        // Placing all of the raids into the display
        for (let raid of raidList) {
            display += `${petinfo.titles[raid[0]]} (LVL: ${raid[1]})\n`
        }

        display += "\`\`\`"

        // Notify admins of the command
        let output = {
            embed: {
                title: "AutoClear Raid List",
                description: display,
                color: constants.colors.white
            },
        };

        bot.customMessage(msg, output, module.exports.options.name, args, false, true);
    }
};
