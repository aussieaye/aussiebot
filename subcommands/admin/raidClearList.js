require(global.baseDir + "/utils/Array.prototype.clean");
require(global.baseDir + "/utils/Date.prototype.toSimpleDate");

const constants = require(global.baseDir + "/config/constants.js");
const notify = require(global.baseDir + "/utils/notify.js");
const petinfo = require(global.baseDir + "/config/petinfo.js");

module.exports = {
    options: {
        parent: "raidclear",
        name: "list",
        usage: "list <# of days>",
        description: "List all tracked raid clears for the current day. If <# of days> is provided, views that many days",
        fullDescription: "List all tracked raid clears for the current day. If <# of days> is provided, views that many days",
        deleteCommand: true,
        hidden: true,
        category: constants.ABC_ADMIN,
        requirements: {
            custom: constants.permissionsCheck.bind({
                permissions: {
                    "administrator": true
                }
            })
        },
        permissionMessage: constants.permissionsMessage
    },

    exec: async function (bot, msg, args) {
        // Removing any empty arguments
        args = args.clean('');

        // Getting a timestamp at the beginning of this day
        let timestamp = new Date;
        timestamp.setHours(0, 0, 0, 0);

        // Checking if there is a valid argument
        if (args.length && !/^[0-9]+$/.test(args[0])) {
            return notify.cmdUsageError(bot, msg, module.exports.options.name, args, "Invalid number of arguments!");
        }

        // Updating timestamp with argument, if exists
        if (args.length) {
            const day = constants.epochDay;
            timestamp.setTime(timestamp.getTime() - ((parseInt(args[0], 10) - 1) * day));
        }

        try {
            // Retrieving all relevant rows
            let query = "SELECT HASH hash, RAID raid, LEVEL level, TIME time FROM CLEARS WHERE TIME >= ? ORDER BY TIME";
            let placeholders = [timestamp.getTime()];
            var rows = await bot.db.all(query, placeholders);
        } catch (e) {
            return notify.cmdInternalError(bot, msg, e, "Failed to query database");
        }

        // Creating the displayed fields
        var display = []

        if (rows.length) {
            let current = "\`\`\`\n";
            let currentDay = new Date(rows[0].time);
            
            rows.forEach((elem) => {
                let tmpDate = new Date(elem.time);
                if (currentDay.getDate() !== tmpDate.getDate()) {
                    // Case where we have moved to a different date
                    current = current.substring(0, current.length - 2); // Removing trailing \n\n
                    current += "\`\`\`"

                    // Creating the field
                    let tmp = {
                        name: currentDay.toSimpleDate(),
                        value: current,
                        inline: false
                    };
                    display.push(tmp);

                    // Updating for future iterations
                    current = "\`\`\`\n";
                    currentDay = tmpDate;
                }

                // Case where there is another clear for that day
                current += `${petinfo.titles[elem.raid]} (LVL: ${elem.level})\n`;
                current += `${elem.hash.substring(0, 8)}\n\n`
            });

            // Finalizing the fields
            current = current.substring(0, current.length - 2); // Removing trailing \n\n
            current += "\`\`\`"

            let tmp = {
                name: currentDay.toSimpleDate(),
                value: current,
                inline: false
            };
            display.push(tmp);
        } else {
            let tmp = {
                name: "\u200b",
                value: "No transactions found for given time period",
                inline: false
            };
            display.push(tmp);
        }

        // Informing admin of command parameters and success
        var output = {
            embed: {
                title: "Raid Clear Transations",
                description: `All raid clear transactions for the past ${args.length ? args[0] : "1"} day(s)`,
                fields: display,
                color: constants.colors.white
            }
        };

        // Notifying admins
        bot.customMessage(msg, output, module.exports.options.name, args, false, true);
    }
};