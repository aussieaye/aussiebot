require(global.baseDir + "/utils/Array.prototype.clean");

const constants = require(global.baseDir + "/config/constants.js");
const notify = require(global.baseDir + "/utils/notify.js");

module.exports = {
    options: {
        parent: "medals",
        name: "adminRemove",
        usage: "adminRemove <transaction ID>",
        description: "Removes the specified transaction from the database",
        fullDescription: "Removes the specified transaction from the database",
        deleteCommand: true,
        hidden: true,
        category: constants.ABC_ADMIN,
        requirements: {
            custom: constants.permissionsCheck.bind({
                permissions: {
                    "administrator": true
                }
            })
        },
        permissionMessage: constants.permissionsMessage
    },

    exec: async function (bot, msg, args) {
        // Removing any empty arguments
        args = args.clean('');

        // Checking the number of arguments
        if (args.length !== 1) {
            return notify.cmdUsageError(bot, msg, module.exports.options.name, args, "Invalid number of arguments!");
        }
        var tid = args[0];

        try {
            // Removing the transaction from the medals table
            let query = "DELETE FROM MEDALS WHERE HASH = ?";
            let placeholders = [tid];
            await bot.db.run(query, placeholders);
        } catch (e) {
            return notify.cmdInternalError(bot, msg, e, "Failed to query database");
        }

        // Informing admin of command parameters and success
        var output = {
            embed: {
                title: "Admin Tracking Event",
                description: `Transaction successfully removed from database`,
                color: constants.colors.white
            }
        };

        // Notify admins
        bot.customMessage(msg, output, module.exports.options.name, args, false, true);
    }
};