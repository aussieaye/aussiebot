require(global.baseDir + "/utils/Array.prototype.clean");

const constants = require(global.baseDir + "/config/constants.js");
const notify = require(global.baseDir + "/utils/notify.js");
const petinfo = require(global.baseDir + "/config/petinfo.js");

module.exports = {
    options: {
        parent: "pets",
        name: "adminSet",
        usage: "adminSet <@mention> <pet identifier> [pet star level] <number of frags>",
        description: "Sets the current number of frags the mentioned player for a given raid pet",
        fullDescription: "Sets the current number of frags the mentioned player has for a given raid pet. <pet identifier> is either the pet's full name or the raid id (e.g. Neru or R3.2). [pet star level] is optional.",
        deleteCommand: true,
        hidden: true,
        category: constants.ABC_ADMIN,
        requirements: {
            custom: constants.permissionsCheck.bind({
                permissions: {
                    "administrator": true
                }
            })
        },
        permissionMessage: constants.permissionsMessage
    },

    exec: async function (bot, msg, args) {
        // Removing any empty arguments
        args = args.clean('');
        let cleanArgs = args.join(", ");

        // Checking the number of arguments
        if (args.length < 3 || args.length > 5) {
            return notify.cmdUsageError(bot, msg, module.exports.options.name, cleanArgs, "Invalid number of arguments!");
        }

        // Making sure there is a mentioned player
        if (!msg.mentions.length && !/^<@[0-9]+>$/.test(args[0])) {
            return notify.cmdUsageError(bot, msg, module.exports.options.name, cleanArgs, "Invalid mention or argument order");
        }
        var user = msg.mentions[0]
        args.shift();

        // Parsing through the arguments
        // Figuring out the raid
        var raidID = args.shift();

        // Checking if we have gotten a raidid directly
        if (!petinfo.raids.includes(raidID.toUpperCase())) {
            // Check if it is single word name
            if (petinfo.names.includes(raidID.toLowerCase())) {
                raidID = petinfo.nameToRaid(raidID);
            } else {
                // Check for a double word name
                raidID += " " + args.shift();
                if (petinfo.names.includes(raidID.toLowerCase())) {
                    raidID = petinfo.nameToRaid(raidID);
                } else {
                    // Could not find the given name
                    raidID = undefined;
                }
            }

            // Making sure we found the pet name/did not fail
            if (!raidID) {
                return notify.cmdArgError(bot, msg, module.exports.options.name, cleanArgs, "Could not find specified pet");
            }
        }
        raidID = raidID.toUpperCase()

        // Parsing the rest of the arguments
        let frags = 0;
        if (args.length === 2) {
            // Handling the case where we got a stars input
            let tmp = args.shift();
            frags = petinfo.stars[tmp]; // undefined if invalid
            if (frags) {
                frags = parseInt(frags, 10);
            }
        }

        if (frags !== undefined && /^[0-9]+$/.test(args[0])) {
            frags += parseInt(args[0], 10);
        } else {
            return notify.cmdArgError(bot, msg, module.exports.options.name, cleanArgs, "Invalid stars or pet fragments value");
        }

        try {
            // Checking that the player is in the database
            let dbID = await bot.db.getPlayerID(user.id);
            if (!dbID) {
                return notify.cmdArgError(bot, msg, module.exports.options.name, args, "Player does not exist in the database");
            }

            // Updating the database entry
            let query = `UPDATE PETS SET '${raidID}' = ? WHERE UID = ?`;
            let placeholders = [frags, dbID];
            await bot.db.run(query, placeholders);
        } catch (e) {
            return notify.cmdInternalError(bot, msg, e, "Failed to query database");
        }

        // Notify admins of the command
        let output = {
            embed: {
                title: "Successfully Updated Pet Frag Count",
                fields: [{
                    name: "Username",
                    value: user.mention,
                    inline: false
                }, {
                    name: "\u200b",
                    value: `\`\`\`\n${petinfo.info[raidID].name} (${raidID}): ${frags}\`\`\``,
                    inline: false
                }],
                color: constants.colors.blue
            },
        };

        // Notifying admins
        bot.customMessage(msg, output, module.exports.options.name, args, false, true);
    }
};