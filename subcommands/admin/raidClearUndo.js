require(global.baseDir + "/utils/Array.prototype.clean");

const constants = require(global.baseDir + "/config/constants.js");
const notify = require(global.baseDir + "/utils/notify.js");
const petinfo = require(global.baseDir + "/config/petinfo.js");

module.exports = {
    options: {
        parent: "raidclear",
        name: "undo",
        usage: "undo <transaction id>",
        description: "Removes the specified transaction ID while also undoing the affects of the clear on all players",
        fullDescription: "Removes the specified transaction ID while also undoing the affects of the clear on all players",
        deleteCommand: true,
        hidden: true,
        category: constants.ABC_ADMIN,
        requirements: {
            custom: constants.permissionsCheck.bind({
                permissions: {
                    "administrator": true
                }
            })
        },
        permissionMessage: constants.permissionsMessage
    },

    exec: async function (bot, msg, args) {
        // Removing any empty arguments
        args = args.clean('');
        var tid = args[0];

        // Checking if there is an argument
        if (args.length !== 1) {
            return notify.cmdArgError(bot, msg, module.exports.options.name, args, "Invalid number of arguments");
        }

        try {
            // Retrieving the relevant row
            let query = "SELECT RAID raid, LEVEL level FROM CLEARS WHERE HASH LIKE ?";
            let placeholders = [tid + "%"];
            var trans = await bot.db.get(query, placeholders);
        } catch (e) {
            return notify.cmdInternalError(bot, msg, e, "Failed to query database");
        }

        // Starting to create displayed value
        let display = "\`\`\`\n"

        // Building the query
        let query = "UPDATE PETS SET ";
        let placeholders = [];
        let regex = new RegExp("^R" + trans.raid + "\\.[1-5]{1}S*$");

        petinfo.raids.forEach((elem) => {
            if (regex.test(elem)) {
                // This is a raid that we care about; add to query
                query += `[${elem}] = [${elem}] - ?, `;
                placeholders.push(petinfo.info[elem].rewards[trans.level - 1]);

                // Updating output
                display += `${petinfo.info[elem].name} (${elem}): -${petinfo.info[elem].rewards[trans.level - 1]}\n`
            }
        });

        // Finish processing the query
        query = query.substring(0, query.length - 2);   // Removes trailing ", "

        try {
            // Running the update query
            await bot.db.run(query, placeholders);

            // Removing the clears transaction
            query = "DELETE FROM CLEARS WHERE HASH LIKE ?";
            placeholders = [tid + "%"];
            await bot.db.run(query, placeholders);
        } catch (e) {
            return notify.cmdInternalError(bot, msg, e, "Failed to query database; POSSIBLE FATAL PROBLEM: CONTANT OWNER!");
        }

        // Finalizing display
        display = display.substring(0, display.length - 1);    // Remove trailing \n
        display += "\`\`\`";

        // Notify admins
        let output = {
            embed: {
                title: "Successfully Removed Raid Clear Transaction",
                fields: [{
                    name: "Removed Transaction ID",
                    value: args[0],
                    inline: false
                }, {
                    name: petinfo.titles[trans.raid],
                    value: display,
                    inline: false
                }],
                color: constants.colors.white
            },
        };

        // Notifying admins
        bot.customMessage(msg, output, module.exports.options.name, args, false, true);
    }
};