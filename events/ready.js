const loadChannels = require(global.baseDir + "/utils/loadChannels.js");
const loadRaids = require(global.baseDir + "/utils/loadRaids.js");

module.exports = function (bot) {
    // Setting the bot status info
    bot.editStatus("Online", bot.status);

    // Loading the channels into the bot
    loadChannels(bot, global.mconf);

    // Loading the raid auto clear info into the bot
    loadRaids(bot, global.mconf);

    bot.logger.info("Bot is ready!");
}
