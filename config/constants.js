const permissionsCheck = require(global.baseDir + "/utils/permissionsCheck.js");
const permissionsMessage = require(global.baseDir + "/utils/permissionsMessage.js");

module.exports = {
    colors: {
        white: parseInt("0xFFFFFF", 16),
        black: parseInt("0x000000", 16),
        blue: parseInt("0x2196F3", 16),
        yellow: parseInt("0xFFEB3B", 16),
        green: parseInt("0x4CAF50", 16),
        orange: parseInt("0xFF9800", 16),
        purple: parseInt("0x9C27B0", 16),
        red: parseInt("0xF44336", 16),
        brown: parseInt("0x795548", 16),
        grey: parseInt("0x9E9E9E", 16)
    },
    ABC_ADMIN: 1,  //AussieBotCommand
    ABC_BOT: 2,
    ABC_GENERAL: 3,
    ABC_NONE: Infinity,
    permissionsMap: {
        strToVal: {
            "admin": 1,
            "bot": 2,
            "general": 3,
            "none": Infinity
        },
        valToStr: {
            1: "admin",
            2: "bot",
            3: "general",
            Infinity: "none",
            null: "none"    // Think I have to do this becuase of how Infinity returns out of a file
        }
    },
    permissionsCheck: permissionsCheck,
    permissionsMessage: permissionsMessage,
    epochDay: 86400000,
    epochMinute: 60000
}