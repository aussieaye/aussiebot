# AussieBot

AussieBot is a bot being built by members of the Aussie Aye guild in Endless Frontier. Its purpose is to easily track data from guild members.

This code is available under the GNU GPL.

# Credits:

General code structure, logger, and help command: https://github.com/perhion/aru

Unf**king my brain on call back shenanigans: https://blogs.msdn.microsoft.com/yizhang/2018/01/17/calling-node-js-sqlite-callback-function-using-promise-and-await/

Process cleanup code: https://stackoverflow.com/questions/14031763/doing-a-cleanup-action-just-before-node-js-exits

Array cleaning: https://stackoverflow.com/questions/281264/remove-empty-elements-from-an-array-in-javascript

Realizing subcommands are a thing: https://github.com/Kraigie/creeeeeeig

Where I got the compliments idea from: https://gist.github.com/blha303/ba6e6117bf8123fbbc78

Environment variable stuff: https://blog.risingstack.com/node-js-project-structure-tutorial-node-js-at-scale/

Walking directories intelligently: https://gist.github.com/VinGarcia/ba278b9460500dad1f50