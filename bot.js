// Checking that the environment variable is defined
["NODE_ENV", ].forEach((name) => {
    if (!process.env[name]){
        throw new Error(`Environment variable ${name} is missing!`)
    }
});

global.baseDir = process.env["NODE_ENV"]

const async = require("async");
const config = require(global.baseDir + "/config/config.json");
const Eris = require("eris");
const notify = require(global.baseDir + "/utils/notify.js");
const register = require(global.baseDir + "/utils/register.js");

// Basic setup of bot
var bot = new Eris.CommandClient(config.discord, config.options, config.commandOptions);

// Finish config setup
global.prefix = config.commandOptions["prefix"];
bot.status = config.status;

// Giving the bot db and logger access
bot.db = require(global.baseDir + "/db/db.js");
bot.logger = require(global.baseDir + "/utils/logger.js");

// Setting up mutable configs
global.mconf = new (require(global.baseDir + "/utils/mconf.js"))();
global.mconf.file("channels", global.baseDir + "/config/channels.json");
global.mconf.file("raids", global.baseDir + "/config/raids.json");
global.mconf.file("commands", global.baseDir + "/config/commands.json");

// Registering events, commands, and subcommands to the bot
async.auto({
    events: function(callback) { register.events(bot, callback); },
    commands: function(callback) { register.commands(bot, callback); },
    subcommands: ['commands', function(callback, results) { register.subcommands(bot, callback); }]
}, (err, results) => {
    if (err) {
        return logger.error(err);
    }
    bot.logger.info("All events and commands are registered!");
});

// Defining customer message sending
bot.customMessage = (original, output, cmd, args, dm = false, admin = false) => {
    if (dm) {
        original.author.getDMChannel()
            .then( function(channel) {
                channel.createMessage(output);

                // If there is a channel, notify the user that a message was sent to them
                if (this.msg.channel.guild) {
                    this.bot.createMessage(this.msg.channel.id, `:mailbox_with_mail: ${this.msg.author.mention}, you've got mail!`);
                }
            }
                .bind( {bot: bot, msg: original} )
            )
            .catch(function(err) {
                this.notify.refError(this.bot, this.original, this.cmd, this.args, err);
            }
                .bind( {notify: notify, bot: bot, original: original, cmd: cmd, args: args} )
            );
    } else if (admin) {
        if (global.mconf.get("channels:admin")) {
            bot.createMessage(global.mconf.get("channels:admin"), output);
        } else {
            if (!original) {
                // Case coming from autoClear where there is no backup
                // just ignore it
                return;
            }
            bot.getDMChannel(original.author.id)
                .then(channel => {
                    channel.createMessage(output);
                })
                .catch(function(err) {
                    this.notify.refError(this.bot, this.original, this.cmd, this.args, err);
                }
                    .bind( {notify: notify, bot: bot, original: original, cmd: cmd, args: args} )
                );
        }
    } else {
        bot.createMessage(original.channel.id, output);
    }
};

// Clean up command
function cleanup(options, err) {
    if (options.cleanup) {
        options.bot.db.dbExit();
        // global.mconf.exit();

        options.bot.logger.info("Bot successfully cleaned up... Now exiting!");
    }

    if (err) {
        console.log(err.stack);
    }

    if (options.exit) {
        process.exit();
    }
}

// Registering cleanup code
// TODO These really don't do anything...
process.on('exit', cleanup.bind(null, {bot: bot, exit: false, cleanup: true}));
process.on('SIGINT', cleanup.bind(null, {bot: bot, exit: true}));      // catches ctrl+c event
process.on('SIGUSR1', cleanup.bind(null, {bot: bot, exit: true}));     // catches "kill pid" (for example: nodemon restart)
process.on('SIGUSR2', cleanup.bind(null, {bot: bot, exit: true}));     // catches "kill pid" (for example: nodemon restart)
process.on('uncaughtException', cleanup.bind(null, {bot: bot, exit: true}));   // catches uncaught exceptions


// Connect to the server
try {
    bot.connect();
} catch (e) {
    console.log("Caught on bot connect!");
    console.log(e);
    return;
}

