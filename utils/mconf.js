require(global.baseDir + "/utils/Object.prototype.isEmpty.js");

const fs = require("mz/fs");

// Tail-recursive function to tunnel to the child object
function getChildObj(keys, obj) {
    if (keys.length > 1 && obj) {
        obj = obj[keys.shift()];
        return getChildObj(keys, obj);
    } else {
        return obj;
    }
}

class mconf {
    constructor() {
        this.files = {};
    }

    file(key, path, manualWrite=false) {
        if (key in this.files) {
            return new Error("key already exists");
        }

        this.files[key] = {
            path: path,
            data: require(path) || {},
            modified: false,
            manualWrite: manualWrite
        }
    }

    async save(key) {
        // Only writing the file if it has been modified
        if (this.files[key].modified) {
            // Writing the file with a callback to handle setting modified parameter
            return fs.writeFile(this.files[key].path, JSON.stringify(this.files[key].data), "utf8")
                .then(function() {
                    this.files[key].modified = false;
                }.bind( {files: this.files, key: key} ))
                .catch(function(err) {
                    console.log(err);
                });
        }
    }

    // Keys are colon separated "<file>:<json key>:..."
    set(key, value, autosave=false) {
        var keys = key.split(":");
        if (keys.length < 1) {
            throw new Error("Invalid set() key");
        }

        var file = keys.shift();
        if (!(file in this.files)) {
            return new Error("No such file");
        }
        
        if (keys.length == 0) {
            // Case where we are writing all of the data
            this.files[file].data = value;
        } else {
            // Case where we are writing a subsection of the data
            var data = this.files[file].data;
            var final = keys.pop();

            for (var i = 0; i < keys.length; i++) {
                var itm = keys[i];
                
                // Making sure that the objects exist
                if (!(itm in data)) {
                    data[itm] = {}
                }

                data = data[itm];
            }

            data[final] = value;
        }
        
        this.files[file].modified = true;
        if (autosave) {
            this.save(file);
        }
    }

    get(key) {
        var keys = key.split(":");
        if (keys.length < 1) {
            throw new Error("Invalid get() key");
        }

        var file = keys.shift();
        if (!(file in this.files)) {
            return undefined;
        }
        
        if (keys.length == 0) {
            return this.files[file].data;
        } else {
            // Case where we are writing a subsection of the data
            var data = this.files[file].data;
            var final = keys.pop();

            for (var i = 0; i < keys.length; i++) {
                var itm = keys[i];
                
                // Making sure that the objects exist
                if (!(itm in data)) {
                    return undefined;
                }

                data = data[itm];
            }

            if (final in data) {
                return data[final];
            } else {
                return undefined;
            }
        }
    }

    async remove(key, force=false) {
        var keys = key.split(":");
        var keys_len = keys.length;
        if (keys_len < 1) {
            throw new Error("Invalid remove() key");
        }

        // Checking if we deleted the file or one of its objects
        if (keys_len == 1) {
            // Case where we are deleting a file
            if (!force && this.files[keys[0]].modified) {
                await this.save(keys[0]);
            }
            delete this.files[keys[0]];
        } else {
            // Case where we are deleting an object from a file
            var file = keys.shift();
            var obj = getChildObj(keys, this.files[file].data);
            delete obj[keys[0]];
            this.files[file].modified = true;
        }
    }

    exists(key, notEmpty=false) {
        var keys = key.split(":");
        if (keys.length < 1) {
            throw new Error("Invalid exists() key");
        }

        var file = keys.shift();
        if (!(file in this.files)) {
            return false;
        }
        
        if (keys.length == 0) {
            if (this.files[file]) {
                return notEmpty ? !this.files[file].isEmpty() : true;
            } else {
                return false;
            }
        } else {
            // Case where we are writing a subsection of the data
            var data = this.files[file].data;
            var final = keys.pop();

            for (var i = 0; i < keys.length; i++) {
                var itm = keys[i];
                
                // Making sure that the next object exist
                if (!(itm in data)) {
                    return false;
                }

                data = data[itm];
            }

            if (final in data) {
                return notEmpty ? !data[final].isEmpty() : true;
            } else {
                return false;
            }
        }
    }

    // TODO fix this in general...
    async exit() {
        // Grabbing a list of all file keys that should be written
        var tmp = []
        for (var key in this.files) {
            if (!this.files[key].manualWrite) {
                // tmp.push(key);
                await this.save(key);
            }
        }

        // Writing any of the modified ones to disk
        // await Promise.all(tmp.map(x => this.save(x)));   //FIXME idk why this doesn't work but i am done fighting it for now
        console.log("Mutable configs cleaned up...");
    }
}

module.exports = mconf;