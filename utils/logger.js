const winston = require("winston")

// Setup winston logger
const logger = new winston.Logger({
    transports: [
        new winston.transports.Console({    // Log for console
            timestamp: true, 
            colorize: true
        }),
        new winston.transports.File({       // Log to file
            filename: "log.log", 
            timestamp: true,
            maxsize: 10000000,
            maxFiles: 2
        })
    ],
    exitOnError: false
});

function info(args) {
    logger.info(args);
}

function error(args) {
    logger.error(args);
}

function warn(args) {
    logger.warn(args);
}

// Export logger
module.exports = {
    logger: logger,
    info: info,
    error: error,
    warn: warn,
}