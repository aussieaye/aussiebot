const constants = require(global.baseDir + "/config/constants.js");

function permissionsCheck(msg) {
    // Expecting this.permissions = {} to be defined if the user needs to be checked
    // Checking if the command is allowed to be run in this channel
    let chanPermission = global.mconf.get(["channels", "list", msg.channel.id].join(":"));
    let cmdPermission = global.mconf.get(["commands", msg.command.label, "category"].join(":"));

    if (chanPermission > cmdPermission) {
        // Not allowed to be here
        return false;
    }
    
    // Checking if the user is allowed to run the command
    if (this.permissions) {
        var authorPermissions = msg.channel.permissionsOf(msg.author.id).json;
        for(var key in this.permissions) {
            if(this.permissions.hasOwnProperty(key) && authorPermissions[key] !== this.permissions[key]) {
                return false;
            }
        }
    }

    // Everything checks out
    return true;
}

module.exports = permissionsCheck