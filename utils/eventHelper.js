const cheerio = require("cheerio");
const rp = require("request-promise");
const schedule = require("node-schedule");

class EventHelper {
    constructor() {
        this.active = {};
        this.uri = 'http://52.52.189.250:8080/EF/getNewsMultiLang?domain=G&lang=EN';
        this.details = "http://52.52.189.250:8080/EF/getNewsDetailMultiLang?domain=G&lang=EN";
        this.threshold = 1000;
        this.refresh();
        this.job = schedule.scheduleJob('0 0 * * *', this.refresh.bind(this));
    }

    async refresh() {
        console.log(`EventHelper is refreshing the cache...`);

        this.active = {}

        var options = {
            uri: this.uri,
            transform: function (body) {
                return cheerio.load(body);
            }
        };

        // Query the news site for the latest info
        var $ = await rp(options);
        let news = {}

        $("[class^='news-img']").each((index, elem) => {
            // Pulling base info
            let href = $(elem).find("a").attr("href");
            let img = $(elem).find("img").attr("src");
            let seq;

            // Grabbing text if it exists
            let text;
            if ($(elem).attr("class") === "news-img-text") {
                text = $(elem).find("div").text();
            }

            // Figuring out the sequence number
            let tmp = href.split("?")[1].split("&");
            for (let param of tmp) {
                param = param.split("=");
                if (param[0] === "seq") {
                    seq = parseInt(param[1]);
                    break;
                }
            }

            // I don't see a reason why this won't always be set, but just as a sanity check
            if (seq) {
                news[seq] = {
                    img: img,
                    text: text,
                    seq: seq
                };
            } else {
                news[href] = {
                    img: img,
                    text: text,
                    seq: seq
                };
                console.log("Something went horribly wrong in EventHelper refresh!");
            }
        });

        // Determining which news items we care about
        let keys = Object.keys(news);
        keys.sort();
        keys.reverse();

        this.seq = [];
        this.news = {};

        for (let i of keys) {
            if (i > this.threshold) {
                this.seq.push(i);
                this.news[i] = news[i];

                // Cleaning/Finalizing the data
                this.news[i].url = this.details + `&seq=${i}`;

                if ("text" in this.news[i] && !this.news[i].text) {
                    delete this.news[i].text;
                }
            }
        }

        console.log(`EventHelper cache refreshed!`);
    }

    newActive(id) {
        if (id in this.active) {
            return undefined;
        }

        this.active[id] = 0;
        let tmp = this.news[this.seq[this.active[id]]];
        tmp.place = this.active[id] + 1;
        return tmp;
    }

    removeActive(id) {
        if (!(id in this.active)) {
            return false;
        }

        delete this.active[id];
        return true;
    }

    getNext(id) {
        if (!(id in this.active)) {
            return undefined;
        }

        this.active[id] += 1;
        if (this.active[id] + 1 === this.seq.length) {
            this.active[id] = 0;
        }

        let tmp = this.news[this.seq[this.active[id]]];
        tmp.place = this.active[id] + 1;
        return tmp;
    }

    getPrev(id) {
        if (!(id in this.active)) {
            return undefined;
        }

        if (this.active[id] === 0) {
            this.active[id] = this.seq.length;
        }
        this.active[id] -= 1;

        let tmp = this.news[this.seq[this.active[id]]];
        tmp.place = this.active[id] + 1;
        return tmp;
    }

    createOutput(current) {
        let output = {
            embed: {
                title: `(${current.place}/${this.seq.length}) Link to event`,
                url: current.url,
                image: {
                    url: current.img
                }
            }
        };

        if (current.text) {
            output.embed.description = current.text;
        }

        return output;
    }
}

module.exports = new EventHelper()