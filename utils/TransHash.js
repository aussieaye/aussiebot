const crypto = require("crypto");

class TransHash {
    constructor(...inputs) {
        this.hasher = new crypto.createHash('sha256');
        this.update(...inputs);
    }

    update(...inputs) {
        inputs.forEach((elem) => {
            this.hasher.update(elem);
        });
    }

    getFull() {
        if (!this.digest) {
            this.digest = this.hasher.digest('hex');
        }
        return this.digest;
    }

    getTrunc() {
        if (!this.digest) {
            this.digest = this.hasher.digest('hex');
        }
        return this.digest.substring(0, 8);
    }
}

module.exports = TransHash