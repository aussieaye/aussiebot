const constants = require(global.baseDir + "/config/constants.js");

function permissionsMessage(msg) {
    // Making sure the message was deleted
    // TODO add? && msg.channel.permissionsOf(msg._client.user.id).has("manageMessages")
    if (!msg.command.deleteCommand && msg.channel.guild) {
        msg.delete(`${msg.command.labl} not allowed to run in ${msg.channel.id}`);
    }

    // Checking if the command was allowed to be run in this channel
    let chanPermission = global.mconf.get(["channels", "list", msg.channel.id].join(":"));
    let cmdPermission = global.mconf.get(["commands", msg.command.label, "category"].join(":"));

    if (chanPermission > cmdPermission) {
        // Not allowed to be here; send DM response
        msg.author.getDMChannel()
            .then(channel => {
                channel.createMessage(`${msg.author.mention}, \`${msg.command.label}\` is not allowed in ${msg.channel.name}`);
            })
            .catch(err => {
                msg.channel.createMessage(`${msg.author.mention}, I could not send you a direct message. Please check your settings.`);
                console.log(err);
            })
        return;
    } else {
        // Command failed for a reason other than the channel settings; return failure string
        return `${msg.author.mention}, you do not have permission to use the \`${msg.command.label}\` command`;
    }
}

module.exports = permissionsMessage