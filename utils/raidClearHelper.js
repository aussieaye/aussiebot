const constants = require(global.baseDir + "/config/constants.js");
const petinfo = require(global.baseDir + "/config/petinfo.js");
const TransHash = require(global.baseDir + "/utils/TransHash.js");

async function raidClearHelper(bot, raid, level, truncate=undefined) {
    // Starting to create displayed value
    let display = "\`\`\`\n"

    // Building the query
    let query = "UPDATE PETS SET ";
    let placeholders = [];
    let regex = new RegExp("^R" + raid + "\\.[1-5]{1}S*$");

    petinfo.raids.forEach((elem) => {
        if (regex.test(elem)) {
            // This is a raid that we care about; add to query
            query += `[${elem}] = [${elem}] + ?, `;
            placeholders.push(petinfo.info[elem].rewards[level - 1]);

            // Updating output
            display += `${petinfo.info[elem].name} (${elem}): +${petinfo.info[elem].rewards[level - 1]}\n`
        }
    });

    // Grabbing a timestamp
    timestamp = new Date;

    // Creating hash of entry for later identification
    let hasher = new TransHash(raid.toString(), level.toString(), timestamp.getTime().toString());

    // Finish processing the query
    query = query.substring(0, query.length - 2);   // Removes trailing ", "

    try {
        // Running the update query
        await bot.db.run(query, placeholders);

        // Updating query info for placing transaction info
        query = "INSERT INTO CLEARS(HASH, RAID, LEVEL, TIME) VALUES(?, ?, ?, ?)";
        placeholders = [hasher.getFull(), raid, level, timestamp.getTime()];
        await bot.db.run(query, placeholders);
    } catch (e) {
        return e;
    }

    // Finalizing display
    display = display.substring(0, display.length - 1);    // Remove trailing \n
    display += "\`\`\`";

    let output;

    if (!truncate) {
        output = {
            embed: {
                title: `${petinfo.titles[raid]} (LVL: ${level}) Cleared\n`,
                description: `\`\`\`\n${timestamp.toUTCString()}\nTransaction ID: ${hasher.getTrunc()}\`\`\``
            },
            color: constants.colors.white
        };
    } else {
        output = {
            embed: {
                title: "Successfully Updated Raid Pet Frag Count",
                description: `Raid cleared by ${truncate.author.mention}`,
                fields: [{
                    name: "Time Registered",
                    value: timestamp.toUTCString(),
                    inline: false
                }, {
                    name: `${petinfo.titles[raid]} (LVL: ${level})\n`,
                    value: display,
                    inline: false
                }, {
                    name: "Transaction ID",
                    value: hasher.getTrunc(),
                    inline: false
                }],
                color: constants.colors.white
            },
        };
    }

    return output;
}

module.exports = raidClearHelper