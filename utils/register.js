const fs = require("fs");
const path = require("path");

var walkSync = function(dir, filelist) {
    if( dir[dir.length-1] != '/') dir=dir.concat('/')
  
    var files = fs.readdirSync(dir);
    filelist = filelist || [];
    files.forEach(function(file) {
      if (fs.statSync(dir + file).isDirectory()) {
        filelist = walkSync(dir + file + '/', filelist);
      }
      else {
        filelist.push(dir + file);
      }
    });
    return filelist;
};

function events(bot, cb) {
    // Attaching events to the bot
    bot.logger.info("Registering events to the bot...");
    var eventPath = path.resolve(global.baseDir, "./events");
    fs.readdir(eventPath, (err, files) => {
        if (err) {
            return bot.logger.error(err);
        }

        // Going through each file
        files.forEach(file => {
            // Checking that this is a file we care about
            if (file.split(".")[1] !== "js") {
                bot.logger.warn(`Ignoring file ${file}!`);
                return;
            }

            bot.logger.info(`Loading event ${file}...`)

            // Loading file
            let eventFile = require(eventPath + "/" + file);

            // Registering the event
            bot.on(file.split(".")[0], (...args) => {
                // Runs the event file
                eventFile(bot, ...args);
            })
        });
    })
    bot.logger.info("Events registered!");
    cb(null, null);
}

function commands(bot, cb) {
    // Attaching commands to the bot
    bot.logger.info("Registering commands to the bot...");
    var cmdPath = path.resolve(global.baseDir, "./commands");

    let files = walkSync(cmdPath);
    files.forEach((file) => {
        // Checking that this is a file we care about
        if (file.split(".")[1] !== "js") {
            bot.logger.warn(`Ignoring file ${file}!`);
            return;
        }

        bot.logger.info(`Loading command ${file}...`);

        // Loading file
        let cmdFile = require(file);

        // Registering the command
        bot.registerCommand(cmdFile.options.name, (msg, args) => { cmdFile.exec(bot, msg, args); }, cmdFile.options);
        
        // Making sure the command permissions are in the configs
        let tester = global.mconf.get("commands:" + cmdFile.options.name.toLowerCase());
        if (!tester || !tester.category) {
            global.mconf.set("commands:" + cmdFile.options.name.toLowerCase(), {category: cmdFile.options.category}, true);
        }
    });

    bot.logger.info("Commands registered!");
    cb(null, null);
}

function subcommands(bot, cb) {
    // Attaching subcommands to the bot
    bot.logger.info("Registering subcommands to the bot...");
    var subcmdPath = path.resolve(global.baseDir, "./subcommands");

    let files = walkSync(subcmdPath);
    files.forEach((file) => {
        // Checking that this is a file we care about
        if (file.split(".")[1] !== "js") {
            bot.logger.warn(`Ignoring file ${file}!`);
            return;
        }

        bot.logger.info(`Loading subcommand ${file}...`);

        // Loading file
        let subcmdFile = require(file);

        // Determining which command is its parent
        let parent = bot.commands[subcmdFile.options.parent];

        // Registering the command
        parent.registerSubcommand(subcmdFile.options.name, (msg, args) => { subcmdFile.exec(bot, msg, args); }, subcmdFile.options);

        // Making sure the command permissions are in the configs
        let key = "commands:" + [subcmdFile.options.parent, subcmdFile.options.name.toLowerCase()].join(":");
        if (!global.mconf.get(key)) {
            global.mconf.set(key, {category: subcmdFile.options.category}, true);
        }
    });

    bot.logger.info("Subcommands registered!");
    cb(null, null);
}

module.exports = {
    events: events,
    commands: commands,
    subcommands: subcommands
}