const constants = require(global.baseDir + "/config/constants.js");
const eventHelper = require(global.baseDir + "/utils/eventHelper.js");

function handleReaction(bot, msg, emoji, userid) {
    // Checking if the message is and object or message type
    if (Object.getPrototypeOf(msg) === Object.prototype) {
        msg = bot.getMessage(msg.channel, msg.id);
    }

    // Checking if the bot made the changes
    if (bot.user.id === userid) {
        return;
    }


    // Checking if the message is within the time constraints
    let now = new Date();
    if (now.getTime() - msg.timestamp > constants.epochMinute * 10) {
        // We are considering the message dead; delete the message
        msg.delete();
    }

    // Figuring out what to display
    let current;
    if (emoji.name === '⬆') {
        current = eventHelper.getPrev(msg.id)
    } else if (emoji.name === '⬇') {
        current = eventHelper.getNext(msg.id);
    } else if (emoji.name === '❌') {
        msg.delete();
        return;
    }

    // Remove the emoji that was posted
    let emoji_str = emoji.id ? emoji.name + ":" + emoji.id : emoji.name;
    msg.removeReaction(emoji_str, userid);

    // Checking if the message existed within the eventHelper
    if (!current) {
        return;
    }

    msg.edit(eventHelper.createOutput(current));
}

module.exports = handleReaction;