const constants = require(global.baseDir + "/config/constants.js");

// TODO Fix incorrect footer recommendations when failing on subcommand

function cmdInternalError(bot, msg, err, reason) {
    bot.logger.error(err);
    bot.createMessage(msg.channel.id, {
        embed: {
            title: "Internal Command Error",
            description: `The specified command failed to process. If this issue persist, please contact an admin!`,
            fields: [{
                name: "Original Message",
                value: msg.content,
                inline: false
            }, {
                name: "Failure Reason",
                value: reason,
                inline: false
            }],
            color: constants.colors.red
        },
    });
}

function cmdArgError(bot, msg, cmdName, args, reason) {
    bot.logger.warn(`WARN: ${cmdName} used by ${msg.author.username}#${msg.author.discriminator} in ${msg.channel.guild.name}#${msg.channel.name} with args ${args} - ${reason}`);
    bot.createMessage(msg.channel.id, {
        embed: {
            title: "Command Argument Error",
            description: `The specified command failed due to incorrect arguments!`,
            fields: [{
                name: "Original Message",
                value: msg.content,
                inline: false
            }, {
                name: "Failure Reason",
                value: reason,
                inline: false
            }],
            footer: {
                text: `See !help | !help ${cmdName}`
            },
            color: constants.colors.yellow
        },
    });
}

function cmdUsageError(bot, msg, cmdName, args, reason) {
    bot.logger.error(`FAILURE: ${cmdName} used by ${msg.author.username}#${msg.author.discriminator} in ${msg.channel.guild.name}#${msg.channel.name} with args ${args} - ${reason}`);
    
    bot.createMessage(msg.channel.id, {
        embed: {
            title: "Command Usage Error",
            description: `There was a problem with how the specified command was used!`,
            fields: [{
                name: "Original Message",
                value: msg.content,
                inline: false
            }, {
                name: "Failure Reason",
                value: reason,
                inline: false
            }],
            footer: {
                text: `See !help | !help ${cmdName}`
            },
            color: constants.colors.yellow
        },
    });
}

function refError(bot, msg, cmdName, args, err) {
    bot.logger.warn(`WARN: Could not message user ${msg.author.username}#${msg.author.discriminator} after they use ${cmdName} in ${msg.channel.guild.name}#${msg.channel.name} with args ${args}\nPROBLEM: ${err}`);
    
    bot.createMessage(msg.channel.id, {
        embed: {
            title: "Could Not Message User",
            description: `There was a problem messaging the author! NOTE the command may have still completed!`,
            fields: [{
                name: "Original Message",
                value: msg.content,
                inline: false
            }],
            footer: {
                text: "Make sure that I have permission to message you!"
            },
            color: constants.colors.grey
        },
    });
}

module.exports ={
    cmdInternalError: cmdInternalError,
    cmdArgError: cmdArgError,
    cmdUsageError: cmdUsageError,
    refError: refError
}