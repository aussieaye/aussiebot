function EFToInt(efnum) {
    // Exponential number
    var expo = 0;
    
    // Checking if there is a character at the end of the efnum
    if (/[a-z]$/.test(efnum)) {
        expo = (efnum.slice(-1).charCodeAt() - 96) * 3;     // Remove char; charToInt; 'a' - 96 = 1; * 10^3
        efnum = efnum.substring(0, efnum.length - 1);
    }

    // Returning the number in javascript form
    return parseFloat(efnum, 10) * (10 ** expo);
}

function computeGrowth(original, updated, decimals = 2) {
    // Check if we need to convert the numbers
    if (typeof original !== "number") {
        original = EFToInt(original);
    }
    if (typeof updated !== "number") {
        updated = EFToInt(updated);
    }

    // Doing the calculation in steps for readability
    var result = updated - original;
    result /= original;
    result *= 100;
    return result.toFixed(decimals) + "%";
}

module.exports = {
    EFToInt: EFToInt,
    computeGrowth: computeGrowth
}