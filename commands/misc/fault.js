require(global.baseDir + "/utils/Array.prototype.clean");

const constants = require(global.baseDir + "/config/constants.js");

module.exports = {
    options: {
        name: "fault",
        usage: "fault",
        description: "Displays whose fault it is",
        fullDescription: "Displays whose fault it is",
        hidden: true,
        deleteCommand: true,
        category: constants.ABC_GENERAL,
        requirements: {
            custom: constants.permissionsCheck
        },
        permissionMessage: constants.permissionsMessage
    },

    exec: function (bot, msg, args) {
        bot.createMessage(msg.channel.id, "It is <@312891464336277505>'s fault.");
    }
};