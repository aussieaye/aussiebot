const channelUsers = require(global.baseDir + "/utils/channelUsers.js");
const constants = require(global.baseDir + "/config/constants.js");
const notify = require(global.baseDir + "/utils/notify.js");
const rp = require("request-promise");

module.exports = {
    options: {
        name: "yomamma",
        aliases: ["yomama"],
        usage: "yomamma [@mention]",
        description: "Have the bot say a 'yo mamma' joke. If a @mention is given, the 'yo mamma' joke is directed towards them.",
        fullDescription: "Have the bot say a 'yo mamma' joke. If a @mention is given, the 'yo mamma' joke is directed towards them.",
        deleteCommand: true,
        category: constants.ABC_GENERAL,
        requirements: {
            custom: constants.permissionsCheck
        },
        permissionMessage: constants.permissionsMessage
    },

    exec: async function (bot, msg, args) {
        // Generating a compliment
        var output = await rp({uri: "http://api.yomomma.info/", json: true});
        output = output.joke;

        // Determining a target
        var user;
        if (msg.mentions.length) {
            user = msg.mentions[0];
        } else {
            // Picking a random user
            let users = channelUsers(msg, true, false);
            if (!users) {
                return notify.cmdInternalError(bot, msg, "Failed to create list of users", "Could not create list of valid users");
            }
            user = users[Math.floor(Math.random() * users.length)];
        }

        // Making the first character lower case to make it flow slightly better
        output = output.charAt(0).toLowerCase() + output.slice(1);
        output = user.mention + ", " + output;

        // Notify channel
        bot.createMessage(msg.channel.id, {
            content: output,
            embed: {
                author: {
                    name: msg.author.username,
                    icon_url: msg.author.staticAvatarURL
                },
            }
        });
    }
};