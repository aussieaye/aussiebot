const constants = require(global.baseDir + "/config/constants.js");
const notify = require(global.baseDir + "/utils/notify.js");

module.exports = {
    options: {
        name: "setAdminOutput",
        usage: "setAdminOutput",
        description: "Sets the bot's admin channel to the channel which this command is issued in",
        fullDescription: "Sets the bot's admin channel to the channel which this command is issued in",
        hidden: true,
        category: constants.ABC_ADMIN,
        requirements: {
            custom: constants.permissionsCheck.bind({
                permissions: {
                    "administrator": true
                }
            })
        },
        permissionMessage: constants.permissionsMessage
    },

    exec: async function (bot, msg, args) {
        // Setting admin channel in config
        global.mconf.set("channels:admin", msg.channel.id, true);

        bot.createMessage(msg.channel.id, {
            embed: {
                title: "Successfully Set Admin Channel",
                description: "This channel is now (probably) the specified admin channel",
                color: constants.colors.white
            },
        });
    }
}