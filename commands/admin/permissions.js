require(global.baseDir + "/utils/Array.prototype.clean");

const constants = require(global.baseDir + "/config/constants.js");
const notify = require(global.baseDir + "/utils/notify.js");

module.exports = {
    options: {
        name: "permissions",
        alias: ["permission"],
        usage: "permissions #<channel> <permission>",
        description: "Defines permissions for channels",
        fullDescription: "Defines permissions for channels",
        deleteCommand: true,
        hidden: true,
        category: constants.ABC_ADMIN,
        requirements: {
            custom: constants.permissionsCheck.bind({
                permissions: {
                    "administrator": true
                }
            })
        },
        permissionMessage: constants.permissionsMessage
    },

    exec: async function (bot, msg, args) {
        // Removing any empty arguments
        args = args.clean('');

        // Checking the number of arguments
        if (args.length !== 2) {
            return notify.cmdUsageError(bot, msg, module.exports.options.name, args, "Invalid number of arguments!");
        }

        // Checking the channel mentions and position
        if (msg.channelMentions.length != 1) {
            return notify.cmdUsageError(bot, msg, module.exports.options.name, args, "Invalid number of channel mentions");
        }
        let chanID = msg.channelMentions[0];

        // Finding permission setting
        let permission = args[0].includes(chanID) ? args[1].toLowerCase() : args[0].toLowerCase();

        // Checking the permissions setting is valid
        if (!constants.permissionsMap.strToVal[permission]) {
            return notify.cmdUsageError(bot, msg, module.exports.options.name, args, "Invalid permissions string");
        }

        // Setting the channel's permission
        global.mconf.set("channels:list:" + chanID, constants.permissionsMap.strToVal[permission], true);

        // Notify channel
        let output = {
            embed: {
                title: "Successfully Updated Channel Permission",
                description: `<#${chanID}> permission set to: ${permission.toUpperCase()}`,
                color: constants.colors.white
            },
        };

        bot.customMessage(msg, output, module.exports.options.name, args, false, true);
    }
}