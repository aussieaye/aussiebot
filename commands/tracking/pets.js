require(global.baseDir + "/utils/Array.prototype.clean");

const constants = require(global.baseDir + "/config/constants.js");
const notify = require(global.baseDir + "/utils/notify.js");
const petinfo = require(global.baseDir + "/config/petinfo.js");

// TODO Raid pets should not exceed the 5* cap

module.exports = {
    options: {
        name: "pets",
        aliases: ["pet"],
        usage: "pets <@mention>",
        description: "Displays the number of pet fragments that the specified player has",
        fullDescription: "Displays the number of pet fragments that the specified player has. If not player is mentioned, the author is used",
        category: constants.ABC_BOT,
        requirements: {
            custom: constants.permissionsCheck
        },
        permissionMessage: constants.permissionsMessage
    },

    exec: async function (bot, msg, args) {
        // Removing any empty arguments
        args = args.clean('');

        // Figuring out the user we are tracking
        if (msg.mentions.length === 1) {
            var user = msg.mentions[0];
        } else {
            var user = msg.author;
        }

        try {
            // Checking if player is already in the database
            var dbID = await bot.db.getPlayerID(user.id);
            if (!dbID) {
                return notify.cmdArgError(bot, msg, module.exports.options.name, args, "Player does not exist in the database");
            }

            // Getting a list of all of the user's current pets
            let query = "SELECT * FROM PETS WHERE UID = ?";
            let placeholders = [dbID];
            var curPets = await bot.db.get(query, placeholders);
        } catch (e) {
            return notify.cmdInternalError(bot, msg, e, "Failed to query database");
        }

        // Processing the pets list into a fields list for display
        var raidTitles = []
        petinfo.titles.forEach((elem) => {
            raidTitles.push(new Array(elem));
        });

        petinfo.raids.forEach((elem) => {
            let tmp = `${petinfo.info[elem].name} (${elem}): ${petinfo.fragsToStars(curPets[elem])}\n`

            // Checking for the resistance pets
            if (!/[0-9]+/.test(elem)) {
                raidTitles[0].push(tmp);
            } else {
                // Dealing with the rest of the raid ids
                raidTitles[parseInt(elem.substring(1, 2), 10)].push(tmp);
            }
        });

        // Parsing the information into fields
        var fields = [{
            name: "User",
            value: user.mention,
            inline: false
        }];

        raidTitles.forEach((elem) => {
            let name = elem.shift();
            let value = elem.join().replace(/,/g, '');   // have to replace commas because JS is dumb
            value = value.substring(0, value.length - 1);   // Removing the ending \n character

            let tmp = {
                name: name,
                value: `\`\`\`\n${value}\`\`\``,
                inline: false
            };
            fields.push(tmp);
        });

        // Informing user of command parameters and success
        msg.channel.createMessage({
            embed: {
                title: "Player Pet Listing",
                description: "Specified player currently owns the following pet fragments:",
                fields: fields,
                color: constants.colors.blue
            },
        });
    }
};