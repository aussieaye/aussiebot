require(global.baseDir + "/utils/Array.prototype.clean");

const constants = require(global.baseDir + "/config/constants.js");
const growth = require(global.baseDir + "/utils/growth.js");
const notify = require(global.baseDir + "/utils/notify.js");

module.exports = {
    options: {
        name: "player",
        usage: "player [@mention]",
        description: "Displays information about the message author or specified user",
        fullDescription: "Displays information about the message author or specified user",
        category: constants.ABC_BOT,
        requirements: {
            custom: constants.permissionsCheck
        },
        permissionMessage: constants.permissionsMessage
    },

    exec: async function (bot, msg, args) {
        // Determining which user we are getting stats for
        var user;
        if (msg.mentions.length === 1) {
            user = msg.mentions[0];
        } else {
            user = msg.author;
        }

        try {
            // Checking if player is already in the database
            var dbID = await bot.db.getPlayerID(user.id);
            if (!dbID) {
                return notify.cmdArgError(bot, msg, module.exports.options.name, args, "Player does not exist in the database");
            }

            // Getting all of the relevant database entries
            // TODO Decide if this should actually be from the beginning of time or not (see first error check below)
            var medalTracks = await bot.db.getMedalData(dbID, 0);
        } catch (e) {
            return notify.cmdInternalError(bot, msg, e, "Failed to query database");
        }

        // Making sure that some data is present
        if (!medalTracks) {
            return notify.cmdArgError(bot, msg, module.exports.options.name, args, "Player has no medals events in the database");
        }

        // Making sure there is more than one event
        if (medalTracks.length <= 1) {
            return notify.cmdArgError(bot, msg, module.exports.options.name, args, "Player only has one medals event in the database");
        }

        // Creating a fields object to be returned
        var fields = [];
        var current = medalTracks.shift();
        
        // Since last tracking event (we know this will work, because there are at least two entries)
        fields.push({
            name: `Since last track event (${new Date(medalTracks[0].time).toDateString()} --> ${new Date(current.time).toDateString()})`,
            value: `${medalTracks[0].val} --> ${current.val}: ${growth.computeGrowth(medalTracks[0].val, current.val)}`,
            inline: false
        });

        // Trying to determine a 3 day growth rate
        var epochThreeDay = constants.epochDay * 3;
        while (medalTracks.length > 1 && current.time - medalTracks[1].time <= epochThreeDay) {
            medalTracks.shift();
        }

        if (current.time - medalTracks[0].time <= epochThreeDay) {
            fields.push({
                name: `Past 3 days (${new Date(medalTracks[0].time).toDateString()} --> ${new Date(current.time).toDateString()})`,
                value: `${medalTracks[0].val} --> ${current.val}: ${growth.computeGrowth(medalTracks[0].val, current.val)}`,
                inline: false
            });
        }

        // Trying to determine a 7 day growth rate
        var epochWeek = constants.epochDay * 7;
        while (medalTracks.length > 1 && current.time - medalTracks[1].time <= epochWeek) {
            medalTracks.shift();
        }

        if (current.time - medalTracks[0].time <= epochWeek) {
            fields.push({
                name: `Past week (${new Date(medalTracks[0].time).toDateString()} --> ${new Date(current.time).toDateString()})`,
                value: `${medalTracks[0].val} --> ${current.val}: ${growth.computeGrowth(medalTracks[0].val, current.val)}`,
                inline: false
            });
        }
        
        // Notify channel
        bot.createMessage(msg.channel.id, {
            embed: {
                title: "Player Info",
                description: `Player information for ${user.mention}`,
                fields: fields,
                color: constants.colors.blue
            },
        });
    }
};