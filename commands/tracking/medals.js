require(global.baseDir + "/utils/Array.prototype.clean");

const constants = require(global.baseDir + "/config/constants.js");
const growth = require(global.baseDir + "/utils/growth.js");
const notify = require(global.baseDir + "/utils/notify.js");
const TransHash = require(global.baseDir + "/utils/TransHash.js");

module.exports = {
    options: {
        name: "medals",
        aliases: ["medal"],
        usage: "medals <@mention> <# of medals> ",
        description: "Tracks the number of medals the mentioned player, or message author, currently has",
        fullDescription: "Tracks the number of medals the mentioned player, or message author, currently has. The order of the mention and number do not matter. Medals values must be in the EF format (e.g. 50e)",
        category: constants.ABC_BOT,
        requirements: {
            custom: constants.permissionsCheck
        },
        permissionMessage: constants.permissionsMessage
    },

    exec: async function (bot, msg, args) {
        // Removing any empty arguments
        args = args.clean('');

        // Checking the number of arguments
        if (args.length < 1 || args.length > 2) {
            return notify.cmdUsageError(bot, msg, module.exports.options.name, args, "Invalid number of arguments!");
        }

        // Figuring out the user we are tracking
        if (msg.mentions.length === 1) {
            // Making sure the author is allowed to use this command
            if (!(msg.author.id === msg.mentions[0].id || msg.channel.permissionsOf(msg.author.id).json["administrator"])) {
                msg.author.getDMChannel()
                .then(channel => {
                    channel.createMessage({
                        embed: {
                            title: "Admin Medal Tracking Failure",
                            description: "You do not have permission to use this command on another user",
                            color: constants.colors.black
                        }
                    });
                })
                .catch(err => {
                    console.log(`Could not send direct message - ${msg}`);
                });
                return;
            }

            var user = msg.mentions[0];
        } else {
            var user = msg.author;
        }

        // Determining where the <# of medals> argument is located
        if (args.length === 2) {
            var count = /<@[0-9]+>/.test(args[0]) ? args[1] : args[0];
        } else {
            var count = args[0];
        }

        // Cleaning the medals value
        count = count.replace(',', '').toLowerCase();

        // Making sure the medals value is valid
        if (!/^[0-9]+\.*[0-9]*[a-z]{0,1}$/.test(count)) {
            return notify.cmdArgError(bot, msg, module.exports.options.name, args, "Format of medals number is invalid");
        }

        try {
            // Checking if player is already in the database
            var dbID = await bot.db.getPlayerID(user.id);
            if (!dbID) {
                return notify.cmdArgError(bot, msg, module.exports.options.name, args, "Player does not exist in the database");
            }
        } catch (e) {
            return notify.cmdInternalError(bot, msg, e, "Failed to query database");
        }

        // Grabbing a timestamp
        timestamp = (new Date).getTime();

        // Creating hash of entry for later identification
        let hasher = new TransHash(user.id, timestamp.toString());

        try {
            // Getting the previous medal tracking event for output use
            let query = "SELECT VALUE val, TIME time FROM MEDALS WHERE UID = ? ORDER BY TIME DESC LIMIT 1";
            let placeholders = [dbID];
            var previous = await bot.db.get(query, placeholders);

            // Placing the new entry into the medals table
            query = "INSERT INTO MEDALS(HASH, VALUE, TIME, UID) VALUES(?, ?, ?, ?)";
            placeholders = [hasher.getFull(), count, timestamp, dbID];
            await bot.db.run(query, placeholders);
        } catch (e) {
            return notify.cmdInternalError(bot, msg, e, "Failed to query database");
        }

        // Creating the output fields
        var fields = [];
        
        fields.push({
            name: "User",
            value: user.mention,
            inline: false
        });

        if (previous) {
            fields.push({
                name: `Growth since previous tracking (${new Date(previous.time).toDateString()} --> ${new Date(timestamp).toDateString()})`,
                value: `${previous.val} --> ${count}: ${growth.computeGrowth(previous.val, count)}`,
                inline: false
            });
        }

        fields.push({
            name: "Transaction ID",
            value: hasher.getTrunc(),
            inline: false
        });

        // Informing user of command parameters and success
        let output = {
            embed: {
                title: "Tracking Event",
                description: "Registered medal tracking event",
                fields: fields,
                color: constants.colors.blue
            }
        };

        // Determine whether to notify admins or user
        if (msg.mentions.length && msg.author.id !== msg.mentions[0].id) {
            output.embed.color = constants.colors.white;
            bot.customMessage(msg, output, module.exports.options.name, args, false, true);
            msg.delete();
        } else {
            bot.customMessage(msg, output, module.exports.options.name, args, true);
        }
    }
};