require(global.baseDir + "/utils/Array.prototype.clean");

const constants = require(global.baseDir + "/config/constants.js");
const notify = require(global.baseDir + "/utils/notify.js");

module.exports = {
    options: {
        name: "start",
        usage: "start",
        description: "Displays useful information to get started using the bot",
        fullDescription: "Displays useful information to get started using the bot",
        category: constants.ABC_BOT,
        requirements: {
            custom: constants.permissionsCheck
        },
        permissionMessage: constants.permissionsMessage
    },

    exec: function (bot, msg, args) {
        // Inform the channel about the bot
        bot.createMessage(msg.channel.id, {
            embed: {
                title: "How to get started using the bot",
                description: "Below are a series of commands to get you started using the bot",
                fields: [{
                    name: "0) You can always refer to the help command for information",
                    value: "```\n!help```",
                    inline: false
                }, {
                    name: "1) Add yourself to the player database",
                    value: "```\n!player add @<yourself>```",
                    inline: false
                }, {
                    name: "2) Try tracking your current number of medals",
                    value: "```\n!medals <# of medals>```",
                    inline: false
                }, {
                    name: "3) View information about your profile (note the message the bot returns!)",
                    value: "```\n!player\nOR\n!pets```",
                    inline: false
                }, {
                    name: "4) Learn more about commands",
                    value: "```\n!help <command>```",
                   inline: false
                }],
                footer: {
                    text: "Refer to the !examples command for exemplary command uses"
                }
            }
        });
    }
};