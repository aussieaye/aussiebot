const constants = require(global.baseDir + "/config/constants.js");
const eventHelper = require(global.baseDir + "/utils/eventHelper.js");

module.exports = {
    options: {
        name: "events",
        aliases: ["event"],
        usage: "events",
        description: "Displays an interactive message detailing the days current events",
        fullDescription: "Displays an interactive message detailing the days current events.",
        deleteCommand: true,
        category: constants.ABC_BOT,
        requirements: {
            custom: constants.permissionsCheck
        },
        permissionMessage: constants.permissionsMessage
    },

    exec: function (bot, msg, args) {
        var output = {
            embed: {
                title: "Today's Events"
            }
        };

        bot.createMessage(msg.channel.id, output)
        .then((new_msg) => {
            // Adding reaction buttons
            new_msg.addReaction('⬆');
            new_msg.addReaction('⬇');
            new_msg.addReaction('❌');

            // Updating the message
            let current = eventHelper.newActive(new_msg.id)
            new_msg.edit(eventHelper.createOutput(current));
        });
    }
};