require(global.baseDir + "/utils/Array.prototype.clean");

const constants = require(global.baseDir + "/config/constants.js");
const notify = require(global.baseDir + "/utils/notify.js");

module.exports = {
    options: {
        name: "channelPermissions",
        usage: "channelPermissions",
        description: "Displays the permissions of the current channel",
        fullDescription: "Displays the permissions of the current channel",
        category: constants.ABC_NONE,
        deleteCommand: true,
        requirements: {
            custom: constants.permissionsCheck
        },
        permissionMessage: constants.permissionsMessage
    },

    exec: function (bot, msg, args) {
        let permissions = global.mconf.get(["channels", "list", msg.channel.id].join(":"));
        permissions = constants.permissionsMap.valToStr[permissions];

        bot.createMessage(msg.channel.id, `${msg.channel.mention} has permissions: ${permissions.toUpperCase()}`);
    }
};